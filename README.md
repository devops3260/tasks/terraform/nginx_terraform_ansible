### Project

This is a simple project that runs our application in AWS. We use Terraform to build infrastraction in AWS and Ansible to deploy the application.

### Steps

1. Create VPC, EC2, ALB in AWS using terraform.
2. Using Ansible to deploy our the application.

For working with Terraform we need AWS Credetials ACCESS_KEY and SECRET_ACCESS_KEY.

```
 export AWS_ACCESS_KEY_ID=**************
 export AWS_SECRET_ACCESS_KEY=**********************************
```

After we add the AWS credentials to our environment. Go to the Terraform directory and run the ```terraform init``` command to load providers and modules, then ```terraform plan``` and ```terraform apply```.

When will the construction of the infrastructure be completed. Go to the Ansible directory and run the ```ansible-playbook playbook.yaml -i inventory ```

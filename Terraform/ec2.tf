######################################
# Data search ami ubuntu linux
#####################################

data "aws_ami" "ubuntu" {
  most_recent = true
  owners      = ["099720109477"]

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }

}


module "vm" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "5.5.0"

  ami = data.aws_ami.ubuntu.id

  name                        = "Demo-nginx"
  instance_type               = "t2.micro"
  key_name                    = tls_private_key.aws_ssh_key.public_key_openssh
  vpc_security_group_ids      = [aws_security_group.nginx.id]
  subnet_id                   = module.vpc.public_subnets[0]
  associate_public_ip_address = true

}

resource "aws_security_group" "nginx" {
  name   = "nginx"
  vpc_id = module.vpc.vpc_id

  ingress {
    description = "SSH port"
    from_port   = "22"
    to_port     = "22"
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "HTTP port"
    from_port   = "80"
    to_port     = "80"
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    "Name" = "nginx-sg"
  }
}
resource "tls_private_key" "aws_ssh_key" {
  algorithm = "RSA"
  rsa_bits  = 4096

}
# resource "aws_key_pair" "key" {
#   key_name   = "dev-key"
#   public_key = file("~/.ssh/id_rsa.pub")
# }



resource "local_file" "hosts_cfg" {
  content = templatefile("inventory.tmpl",
    {
      ubuntu_hosts = [module.vm.public_ip]
    }
  )
  filename = "../Ansible//inventory"
}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 4.46"
    }
    tls = {
      source  = "hashicorp/tls"
      version = "~>3.0"
    }
  }
  required_version = "~> 1.3"
}

variable "aws_region" {
  description = "AWS region"
  default     = "us-east-1"
  type        = string

}
variable "default_tags" {
  type        = map(string)
  description = "Default tags for AWS that will be attached to each resource."
  default = {
    "TerminationDate" = "Permanent",
    "Environment"     = "Development",
    "Team"            = "DevOps",
    "DeployedBy"      = "Terraform",
    "OwnerEmail"      = "devops@example.com"
  }

}

variable "cidr_block" {
  type        = string
  description = "cdir block for VPC"
  default     = "10.10.0.0/16"

}
variable "vpc_name" {
  type        = string
  description = "Name for VPC"
  default     = "myVPC"

}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "5.1.2"

  name = var.vpc_name
  cidr = var.cidr_block

  azs            = data.aws_availability_zones.available.names
  public_subnets = ["10.10.1.0/24", "10.10.2.0/24"]

  enable_nat_gateway   = false
  enable_dns_hostnames = true

  tags = {
    "Name" = "${var.vpc_name}"
  }

  public_subnet_tags = {
    "Name" = "public-subnet-${var.vpc_name}"
  }
}

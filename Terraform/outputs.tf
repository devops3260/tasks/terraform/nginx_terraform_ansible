output "ec2_public_ip" {
  description = "Public ip form Ec2"
  value       = module.vm.public_ip
}
output "dns_name" {
  description = "DNS name form alb"
  value       = module.alb.lb_dns_name
}
output "tls_private_key" {
  value     = tls_private_key.aws_ssh_key.private_key_pem
  sensitive = true
}
